# NETFLAX

Votre netflix pas comme Netflix


# Description

Application monolitique permettant de trouver des idées de films basées sur vos préférences !

Les données utilisées sont les données disponibles via : https://api.themoviedb.org/3/  

API KEY : 01da168085204e063c267a7ae2e93a18

See : https://developers.themoviedb.org/3/getting-started/introduction

## Installation

```
git clone
cd myproject
composer install
```

Créer un mot fichier .env.local et modifier la ligne
```
DATABASE_URL=mysql://user:password@127.0.0.1:3306/db_name?serverVersion=5.7
```

Créer la Base de données
```
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
```

Mettre à jour le schéma
```
php bin/console doctrine:schema:update --force
```
ou alias
```
php bin/console d:s:u --force
```

Installer yarn
```
yarn
```

## Quick Start

Lancer la compilation des assets
```bash
yarn encore dev --watch
```

Lancer le serveur symfony
```
symfony server:start
```


## "Schéma" base de données

```
Table UTILISATEURS
    ID
    PSEUDO

Table GENRES
    ID_GENRE (INT autoincrement base2 -> 1/2/4/8/16/32/64/128/...)
    LIBELLE

Table VISIONAGE
		
    TITRE (Titre du film / série)
    ID_UTILISATEUR
    SOMME_GENRE_FILM (somme des ID des genres du film)
    NOTE (1 à 5)
    LIKE (Boolean)
```


## Exemple de réponse de l'API

VERIFIER AVEC NOUVELLE API

```json
{
	"Title":"John Wick",
	"Year":"2014",
	"Rated":"R",
	"Released":"24 Oct 2014",
	"Runtime":"101 min",
	"Genre":"Action, Crime, Thriller",
	"Director":"Chad Stahelski, David Leitch",
	"Writer":"Derek Kolstad",
	"Actors":"Keanu Reeves, Michael Nyqvist, Alfie Allen, Willem Dafoe",
	"Plot":"An ex-hit-man comes out of retirement to track down the gangsters that killed his dog and took everything from him.",
	"Language":"English, Russian, Hungarian",
	"Country":"USA, UK, China",
	"Awards":"5 wins & 8 nominations.",
	"Poster":"https://m.media-amazon.com/images/M/MV5BMTU2NjA1ODgzMF5BMl5BanBnXkFtZTgwMTM2MTI4MjE@._V1_SX300.jpg",
	"Ratings":
		[{
			"Source":"Internet Movie Database",
			"Value":"7.4/10"
		},
		{
			"Source":"Rotten Tomatoes",
			"Value":"87%"
		},
		{
			"Source":"Metacritic",
			"Value":"68/100"
		}],
	"Metascore":"68",
	"imdbRating":"7.4",
	"imdbVotes":"523,756",
	"imdbID":"tt2911666",
	"Type":"movie",
	"DVD":"N/A",
	"BoxOffice":"N/A",
	"Production":"87eleven, Defynite Films, Thunder Road Pictures",
	"Website":"N/A",
	"Response":"True"
}
```
## DOC DEV

Google Doc

https://docs.google.com/document/d/1hquidnaMiNsC8aTBxNgcXC8W4rcP5BBpKhWpwIUCbhA/edit?usp=sharing

## License

  [MIT](LICENSE)