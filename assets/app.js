/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './scss/app.scss';
import './scss/pages/login.scss';
import './scss/pages/register.scss';
import './scss/pages/films.scss';
import './scss/pages/search.scss';
import './scss/pages/myAccount.scss';
import './scss/pages/tv.scss';
import './scss/components/nav.scss';
import './scss/components/header.scss';
import './scss/components/movie-card.scss';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery';

console.log('Hello Webpack Encore! Edit me in assets/app.js');
