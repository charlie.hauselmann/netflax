<?php

namespace App\Twig;

use App\Service\TmdbApiService;
use Symfony\Component\Asset\Packages;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class AppExtension
 *
 * @package App\Twig
 */
class AppExtension extends AbstractExtension
{
    /** @var TmdbApiService $tmdbApiService */
    private $tmdbApiService;
    private $assets;

    /**
     * AppExtension constructor.
     *
     * @param TmdbApiService $tmdbApiService
     * @param Packages $assets
     */
    public function __construct(TmdbApiService $tmdbApiService, Packages $assets)
    {
        $this->tmdbApiService = $tmdbApiService;
        $this->assets = $assets;
    }
    /**
     * @return array|TwigFilter[]
     */
    public function getFilters()
    {
        return [
            new TwigFilter('displayTmdbImage', [$this, 'displayTmdbImage']),
        ];
    }

    public function displayTmdbImage(?string $path, string $size = 'original', string $type = 'movie')
    {
        return !empty($path) ? $this->tmdbApiService->getImagePath($path, $size) : $this->assets->getUrl('img/default-'. $type .'.png');
    }
}