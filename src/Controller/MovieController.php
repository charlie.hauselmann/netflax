<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Service\TmdbApiService;
use Symfony\Component\Routing\Annotation\Route;


class MovieController extends AbstractController
{
    
    /** @var TmdbApiService $tmdbApiService */
    private $tmdbApiService;

    public function __construct(TmdbApiService $tmdbApiService)
    {
        $this->tmdbApiService  = $tmdbApiService;
    }

    /**
     * @Route("/movie/{id}", name="movie_details")
     */
    public function index(string $id)
    {
        $detail = $this->tmdbApiService->getFilm($id);
        $credits = $this->tmdbApiService->getCredits($id);
        $movies = array_slice($this->tmdbApiService->getSimilar($id)->results, 0, 4);
        if (!empty($userMovies = $this->getUser()->getMovies())) {
            $favs = $userMovies->getFilms();
        } else {
            $favs = [];
        }
        $videos = $this->tmdbApiService->getVideos($id);

        foreach($movies as $movie) {
            foreach($favs as $fav) {
                if($movie->id === $fav) {
                    $movie->{"fav"} = true;
                }
                if($detail->id === $fav) {
                    $detail->{"fav"} = true;
                }
            }
        }

        return $this->render('display/movieDetail.html.twig',
            [
                'detail'  => $detail,
                'credits' => $credits,
                'movies'  => $movies,
                'videos'  => $videos
            ]);
    }
}
