<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Service\TmdbApiService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;


class SearchController extends AbstractController
{
    
    /** @var TmdbApiService $tmdbApiService */
    private $tmdbApiService;

    public function __construct(TmdbApiService $tmdbApiService)
    {
        $this->tmdbApiService  = $tmdbApiService;
        // $this->request  = $request;
    }

    /**
     * @Route("/search", name="app_search")
     */
    public function index(Request $request)
    {
        if($request) {
            // dd($request);
            if (!empty($param = $request->get('searchFieldTitle'))) {
                $movies = $this->tmdbApiService->search($param);
                $favs = $this->getUser()->getMovies()->getFilms();

                foreach($movies as $movie) {
                    foreach($favs as $fav) {
                        if($movie->id === $fav) {
                            $movie->{"fav"} = true;
                        }
                    }
                }

                return $this->render('display/search.html.twig',
                 ['movies' => $movies]); 
            }
        }
        return $this->render('display/search.html.twig'); 
    }

    /**
     * @Route("/discoverByFilter", name="app_discoverFilters")
     */
    public function discoverByFilter(Request $request)
    {
        if($request) {
            // dd($request);
            if (!empty($request->get('searchFieldPeople')) || !empty($request->get('searchFieldGenre'))) {
                $params['sort_by'] = 'popularity.desc';
                $params['with_genres'] = $request->get('searchFieldGenre');
                $params['with_people'] = $request->get('searchFieldPeople');
                $movies = $this->tmdbApiService->getDiscover($params)->results;
                $favs = $this->getUser()->getMovies()->getFilms();

                foreach($movies as $movie) {
                    foreach($favs as $fav) {
                        if($movie->id === $fav) {
                            $movie->{"fav"} = true;
                        }
                    }
                }
                return $this->render('display/search.html.twig',
                 ['movies' => $movies]); 
            }
        }
        return $this->render('display/search.html.twig'); 
    }



     /**
     * @Route("/search/{title}", name="search_movie")
     * @return JsonResponse
     */
    public function searchTitle(string $title)
    {
        $movies = $this->tmdbApiService->search($title);
        // reconstruire un tableau de string avant envoie
        $moviesTitle = [];
        foreach ($movies as $movie) {
            array_push($moviesTitle, $movie->original_title);
        }
        return new JsonResponse([
            'movies' => $moviesTitle
        ]);
    }

    /**
    * @Route("/searchPeople/{people}", name="search_people")
    * @return JsonResponse
    */
   public function searchPeople(string $people)
   {
       $peoples = $this->tmdbApiService->getPersons($people)->results;

       return new JsonResponse([
           'peoples' => $peoples
       ]);
   }

    /**
     * @Route("/searchGenre", name="search_genre")
     * @return JsonResponse
     */
    public function searchGenre(){
    
        $genres = $this->tmdbApiService->getGenres()->genres;

        return new JsonResponse([
            'genres' => $genres
        ]);
    }
}