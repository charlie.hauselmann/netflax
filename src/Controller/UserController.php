<?php

namespace App\Controller;

use App\Entity\Ranking;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Service\TmdbApiService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\UserMovies;

class UserController extends AbstractController
{
    /** @var TmdbApiService $tmdbApiService */
    private $tmdbApiService;

    /** @var EntityManagerInterface $em */
    private $em;

    public function __construct(TmdbApiService $tmdbApiService, EntityManagerInterface $em)
    {
        $this->tmdbApiService = $tmdbApiService;
        $this->em = $em;
    }

    /**
     * @Route("/mes-films", name="app")
     */
    public function index()
    {
        $user = $this->getUser();
        $userPreferences = $this->getUser()->getPreference();
        $params = array_flip($this->tmdbApiService->getDiscoverAvailableFilter());
        $params['sort_by'] = 'popularity.desc';

        $rankings = $this->em->getRepository(Ranking::class)->getRankings($user);

        $prefActors = $this->getTopPrefActors($rankings);
        $prefGenres = $this->getTopPrefGenres($rankings);
        $pondeActor = [8,5,3];

        $movieByActor = [];
        $i = 0;
        $params['with_genres'] = ""; 
        foreach($prefActors as $actor) { 
            $params['with_people'] = (string) $actor->getApiId(); 
            $tmpArray = $this->tmdbApiService->getDiscover($params)->results;
            $y=0;
            foreach($tmpArray as $tmp) {
                if($y < $pondeActor[$i]) {
                    if(is_bool(array_search($tmp, $movieByActor))) {
                        array_push($movieByActor, $tmp);
                        $y++;
                    }
                }
            }
            $i++;
        }
        $params['with_people'] = ''; 
        foreach ($prefGenres as $genre) {
            $params['with_genres'] = (string) $genre->getApiId() .'|';
        }
        $tmpArray = $this->tmdbApiService->getDiscover($params)->results;
        $y=0;
        foreach($tmpArray as $tmp) {
            if(count($movieByActor) < 20) {
                if(is_bool(array_search($tmp, $movieByActor))) {
                    array_push($movieByActor, $tmp);
                    $y++;
                }
            }
        }

        foreach ($userPreferences->getGenre() as $genre) {
            $params['with_genres'] .= $genre->id .'|';
        }
        foreach ($userPreferences->getActor() as $actor) {
            $params['with_people'] .= $actor->id .'|';
        }

        $params['with_genres'] = substr($params['with_genres'], 0, -1);
        $params['with_people'] = substr($params['with_people'], 0, -1);
        $movies = $this->tmdbApiService->getDiscover($params)->results;

        if (!empty($userMovies = $user->getMovies())) {
            $favs = $userMovies->getFilms();

            foreach($movies as $movie) {
                foreach($favs as $fav) {
                    if($movie->id === $fav) {
                        $movie->{"fav"} = true;
                    }
                }
            }
            foreach($movieByActor as $movieA) {
                foreach($favs as $fav) {
                    if($movieA->id === $fav) {
                        $movieA->{"fav"} = true;
                    }
                }
            }
        }

        return $this->render('user/films.html.twig', [
            'recommended' => $movieByActor,
            'movies'      => $movies
        ]);
    }

    private function getTopPrefActors(array $rankings) {
        $prefActors = [];
        usort($rankings, function($a, $b)
        {
            return $a->getWeight() < $b->getWeight();
        });
        foreach($rankings as $ranking) {
            if($ranking->getType() == "actors") {
                if(count($prefActors) < 3) {
                    array_push($prefActors, $ranking);
                }
            }
        }
        return $prefActors;
    }

    private function getTopPrefGenres(array $rankings) {
        $prefGenres = [];
        usort($rankings, function($a, $b)
        {
            return $a->getWeight() < $b->getWeight();
        });
        foreach($rankings as $ranking) {
            if($ranking->getType() == "genres") {
                if(count($prefGenres) < 5) {
                    array_push($prefGenres, $ranking);
                }
            }
        }
        return $prefGenres;
    }

    /**
     * @Route("/mes-films/{page}", name="app_loadmore")
     * @return JsonResponse
     */
    public function loadMore(string $page)
    {
        $user = $this->getUser();
        $userPreferences = $this->getUser()->getPreference();
        $params = array_flip($this->tmdbApiService->getDiscoverAvailableFilter());
        $params['sort_by'] = 'popularity.desc';
        $params['page'] = $page;

        foreach ($userPreferences->getGenre() as $genre) {
            $params['with_genres'] .= $genre->id .'|';
        }
        foreach ($userPreferences->getActor() as $actor) {
            $params['with_people'] .= $actor->id .'|';
        }

        $params['with_genres'] = substr($params['with_genres'], 0, -1);
        $params['with_people'] = substr($params['with_people'], 0, -1);
        $movies = $this->tmdbApiService->getDiscover($params)->results;

        if (!empty($userMovies = $user->getMovies())) {
            $favs = $userMovies->getFilms();

            foreach($movies as $movie) {
                foreach($favs as $fav) {
                    if($movie->id === $fav) {
                        $movie->{"fav"} = true;
                    }
                }
            }
        }

        return new JsonResponse([
            'html' => $this->renderView('components/movie-list.html.twig', [
                'movies' => $movies,
            ])
        ]);
    }

    /**
     * @Route("/profile", name="app_userDetails")
     * @return JsonResponse
     */
    public function profile()
    {
        $user = $this->getUser();
        $userPreferences = $user->getPreference();
        $actors = $userPreferences->getActor();
        $genres = $userPreferences->getGenre();

        return $this->render('display/myAccount.html.twig', [
            'user' => $user,
            'genres' => $genres,
            'actors' => $actors
        ]);
    }

    /**
     * @Route("/userMovies", name="app_userMovies")
     * @return JsonResponse
     */
    public function userMovies()
    {
        $user = $this->getUser();
        $userMoviesId = $user->getMovies();
        $userMovies = [];
        if (!empty($userMoviesId)){
            $userMoviesId = $userMoviesId->getFilms();
            foreach($userMoviesId as $movieId) {
                $movie = $this->tmdbApiService->getFilm($movieId);
                $movie->{"fav"} = true;
                array_push($userMovies, $movie);
            }
        }
        return $this->render('display/userMovies.html.twig', [
            'movies' => $userMovies,
        ]);

    }

    /**
     * @Route("/addFav/{id}", name="app_addFav")
     * @return JsonResponse
     */
    public function addFavorite(int $id)
    {
        $uMoves = new UserMovies();
        $user = $this->getUser();

        if (!empty($userMovies = $user->getMovies())) {
            $userMovies = $userMovies->getFilms();
        } else {
            $userMovies = [];
        }
        $i = false;
        foreach($userMovies as $tmp) {
            if((int) $tmp != $id && $i==false) {
                $i=false;
            } else {
                $i=true;
            }
        }
        if($i == false) {
            array_push($userMovies, $id);
        }
        $uMoves->setFilms($userMovies);
        $user->setMovies($uMoves);

        $movie =  $this->tmdbApiService->getFilm($id);
        $actor = $this->tmdbApiService->getCredits($id);
        if (!empty($actor->cast)) {
            $movie->actors = $actor->cast;
        }

        foreach (['genres', 'actors'] as $type) {
            foreach ($movie->{$type} as $item) {
                /** @var Ranking $ranking */
                $ranking = $this->em->getRepository(Ranking::class)->findOneBy(
                    [
                        'user'  => $user,
                        'apiId' => $item->id,
                        'type'  => $type
                    ]
                );

                if (empty($ranking)) {
                    $ranking = new Ranking();
                    $ranking
                    ->setUser($user)
                    ->setType($type)
                    ->setWeight(1)
                    ->setApiId($item->id);

                    $this->em->persist($ranking);
                } else {
                    $ranking->setWeight($ranking->getWeight() + 1);
                }
            }
        }

        $this->em->flush();

        return new JsonResponse([
            "id" => $id
        ]);
    }

    /**
     * @Route("/removeFav/{id}", name="app_removeFav")
     * @return JsonResponse
     */
    public function removeFavorite(int $id)
    {
        $uMoves = new UserMovies();
        $user = $this->getUser();
        $userMovies = $user->getMovies()->getFilms();

        $i = 0;
        $y = false;
        foreach($userMovies as $tmp) {
            if((int) $tmp != $id && $y==false) {
                $i++;
            } else {
                $y = true;
            }
        }
        array_splice($userMovies, $i, 1);
        // $userMovies = [];
        $uMoves->setFilms($userMovies);
        $user->setMovies($uMoves);
        
        $movie =  $this->tmdbApiService->getFilm($id);
        $actor = $this->tmdbApiService->getCredits($id);
        if (!empty($actor->cast)) {
            $movie->actors = $actor->cast;
        }

        foreach (['genres', 'actors'] as $type) {
            foreach ($movie->{$type} as $item) {
                /** @var Ranking $ranking */
                $ranking = $this->em->getRepository(Ranking::class)->findOneBy(
                    [
                        'user'  => $user,
                        'apiId' => $item->id,
                        'type'  => $type
                    ]
                );
                if(!empty($ranking)) {
                    $weight = $ranking->getWeight();
                    if ($weight == 0) {
                        $this->em->remove($ranking);
                    } else {
                        $ranking->setWeight($ranking->getWeight() - 1);
                    }
                }
            }
        }
        $this->em->flush();

        return new JsonResponse([
            "id" => $movie->id
        ]);
    }
}