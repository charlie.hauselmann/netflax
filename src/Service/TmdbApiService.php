<?php

namespace App\Service;

use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class TmdbApiService
 * @package App\Service
 */
class TmdbApiService
{
    private const API_KEY = '01da168085204e063c267a7ae2e93a18';    // TMDB API KEY
    private const API_URL_SUFFIX = 'https://api.themoviedb.org/3'; // TMDB API URL

    private const API_IMAGE_BASE = 'http://image.tmdb.org/t/p';

    public const FORM_NAME = 'tmdb_api';

    private $urlPart;        // Part of request URL
    private $parameters;     // Request parameters
    private $body;           // Request body
    private $guestSessionId; // Guest session ID used for rating

    /** @var Security $security */
    private $security;

    /**
     * TmdbApiService constructor.
     * @param Security $security
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function getUser(): ?UserInterface
    {
        return $this->security->getUser();
    }

    /**
     * @param string $method
     * @return mixed
     */
    public function callApi($method = 'get')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::API_URL_SUFFIX . $this->urlPart . '?' . (!empty($this->parameters) ? http_build_query($this->parameters) . '&' : '') . 'api_key=' . self::API_KEY);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if ($method == 'post') {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->body));
        }

        $headers = [];
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response);
    }

    /**
     * @param int $film
     * @return mixed|null
     */
    public function getFilm(int $film)
    {
        $this->urlPart = '/movie/' . $film;

        return $this->callApi();
    }

    /**
     * @param int $film
     * @return mixed|null
     */
    public function getSimilar(int $film)
    {
        $this->urlPart = '/movie/' . $film . '/similar';

        return $this->callApi();
    }

    /**
     * @param string $search
     * @return mixed
     */
    public function search(string $search)
    {
        $this->urlPart = '/search/movie';

        $this->parameters = [
            'page'  => 1,
            'query' => $search
        ];

        return $this->callApi()->results;
    }

    /**
     * @param string $suffixPath
     * @param string $size
     * @return string
     */
    public function getImagePath(string $suffixPath, string $size = 'original')
    {
        $suffixPath = (substr($suffixPath, 0, 1) === '/' ? '' : '/') . $suffixPath;
        return self::API_IMAGE_BASE . '/' . $size . $suffixPath;
    }

    /**
     * @return mixed
     */
    public function getGenres()
    {
        $this->urlPart = '/genre/movie/list';

        return $this->callApi();
    }

    /**
     * @return mixed
     */
    public function getPopularPersons()
    {
        $this->urlPart= '/person/popular';

        return $this->callApi();
    }

    /**
     * @return mixed
     */
    public function getPersons(string $param)
    {
        $this->urlPart= '/search/person';

        $this->parameters = [
            'page'  => 1,
            'query' => $param
        ];

        return $this->callApi();
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function getDiscover(array $params)
    {
        $this->urlPart = '/discover/movie';
        $this->parameters = $params;

        return $this->callApi();
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function getDiscoverSearch(string $genres, string $people)
    {
        $this->urlPart = '/discover/movie';
        $this->parameters = $params;

        return $this->callApi();
    }

    /**
     * @return array
     */
    public function getDiscoverAvailableFilter()
    {
        return ['sort_by', 'with_genres', 'with_people'];
    }

    public function getCredits(int $id)
    {
        $this->urlPart = '/movie/'. $id .'/credits';
        return $this->callApi();
    }

    public function getVideos($id)
    {
        $this->urlPart = '/movie/'. $id .'/videos';
        return $this->callApi();
    }
}