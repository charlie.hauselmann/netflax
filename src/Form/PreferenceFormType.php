<?php

namespace App\Form;

use App\Entity\Preference;
use App\Service\TmdbApiService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RegistrationType
 *
 * @package App\Form
 */
class PreferenceFormType extends AbstractType
{
    /** @var TmdbApiService $tmdbApiService **/
    private $tmdbApiService;

    /**
     * @param TmdbApiService $tmdbApiService
     */
    public function __construct(TmdbApiService $tmdbApiService)
    {
        $this->tmdbApiService = $tmdbApiService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $genres = $this->tmdbApiService->getGenres()->genres;
        $actors = $this->tmdbApiService->getPopularPersons()->results;

        $builder
            ->add('actor', ChoiceType::class, [
                'choices'  => $actors,
                'choice_label' => 'name',
                'choice_value' => 'id',
                'choice_attr'  => function($actor) {
                return [
                    'data-image' => $actor->profile_path
                ];
                },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('genre', ChoiceType::class, [
                'choices'  => $genres,
                'choice_label' => 'name',
                'choice_value' => 'id',
                'multiple' => true,
                'expanded' => true,
            ])
        ;
    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'         => Preference::class,
            'translation_domain' => 'messages',
        ]);
    }
}