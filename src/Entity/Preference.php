<?php

namespace App\Entity;

use App\Repository\PreferenceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PreferenceRepository::class)
 */
class Preference
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="array")
     */
    private $genre = [];

    /**
     * @ORM\Column(type="array")
     */
    private $actor = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGenre(): ?array
    {
        return $this->genre;
    }

    public function setGenre(array $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    public function getActor(): ?array
    {
        return $this->actor;
    }

    public function setActor(array $actor): self
    {
        $this->actor = $actor;

        return $this;
    }
}
