<?php 
    $array = array();
    $responseCode = 500; /* si la requête est bien en Ajax et la méthode en GET ... */

    $API_KEY = '01da168085204e063c267a7ae2e93a18';    // TMDB API KEY
    $API_URL_SUFFIX = 'https://api.themoviedb.org/3'; // TMDB API URL

    if((strtolower(filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest') && ($_SERVER['REQUEST_METHOD'] == 'GET')){
        
        dd($_REQUEST['title']);
        /* on récupère le terme et on le duplique en terme en transformant les espaces en tirets et tirets en espaces (au cas ou) */
        $q = str_replace("''","'",urldecode($_REQUEST['title']));
        $q = strtolower(str_replace("'","''",$q));
        $qTiret = str_replace(' ','-',$q);
        $qSpace = str_replace('-',' ',$q);
        $array = array();

        $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, self::API_URL_SUFFIX . $this->urlPart . '?' . (!empty($this->parameters) ? http_build_query($this->parameters) . '&' : '') . 'api_key=' . self::API_KEY);
        curl_setopt($ch, CURLOPT_URL, 'https://api.themoviedb.org/3/search/movie?api_key=01da168085204e063c267a7ae2e93a18&language=en-US&page=1&include_adult=false&query='.$q);
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $headers = [];
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);
        curl_close($ch);

        $responseCode = 200; 
    }

    /* génération réponse JSON */
    http_response_code($responseCode);
    header('Content-Type: application/json');
    echo $response; 
 ?>